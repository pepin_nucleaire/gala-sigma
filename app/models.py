from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from . import db, login
from datetime import datetime

class User(UserMixin, db.Model):

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), index=True, unique=True, nullable=False)
    password = db.Column(db.String(256), nullable=False)
    email_confirmation_sent_on = db.Column(db.DateTime, nullable=True)
    email_confirmed = db.Column(db.Boolean, nullable=True, default=False)
    email_confirmed_on = db.Column(db.DateTime, nullable=True)
    admin = db.Column(db.Boolean, nullable=False, default=False)

    nom = db.Column(db.String(256), nullable=True)
    prenom = db.Column(db.String(256), nullable=True)
    diplome = db.Column(db.Integer, nullable=False, default=False)
    places_prises = db.Column(db.Integer, default=0)
    def __init__(self, email, password, prenom, nom, email_confirmation_sent_on=None):
        self.email = email
        self.password = generate_password_hash(password)
        self.email_confirmation_sent_on = email_confirmation_sent_on
        self.email_confirmed = False
        self.email_confirmed_on = None
        self.prenom = prenom
        self.nom = nom
        self.admin = False
        self.email_confirmation_sent_on = datetime.now()

    def __repr__(self):
        return '<Email {}>'.format(self.email)
    
    def __str__(self):
        return self.prenom + " " + self.nom

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def set_seats_taken(self, nbplaces_str):
        """
        get and set the number of seats taken in db
        """
        places_user = int(self.places_prises)
        places_user += int(nbplaces_str)
        self.places_prises = str(places_user)

    def can_take_places(self):
        """
         Check whether the user has seats available
        """
        if int(self.places_prises) == 10:
            return False
        else:
            return True

    def set_seats_removed(self, places_removed):
        places_user = int(self.places_prises)
        self.places_prises = places_user - places_removed

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id'         : self.id,
            'nom' : self.prenom + " " + self.nom,
            'nombre_places_prises' : self.places_prises
        }

@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class PlanDeTable(db.Model):
    __tablename__ = 'plan'

    id = db.Column(db.Integer, primary_key=True)
    place1 = db.Column(db.Integer, db.ForeignKey('users.id'))
    place2 = db.Column(db.Integer, db.ForeignKey('users.id'))
    place3 = db.Column(db.Integer, db.ForeignKey('users.id'))
    place4 = db.Column(db.Integer, db.ForeignKey('users.id'))
    place5 = db.Column(db.Integer, db.ForeignKey('users.id'))
    place6 = db.Column(db.Integer, db.ForeignKey('users.id'))
    place7 = db.Column(db.Integer, db.ForeignKey('users.id'))
    place8 = db.Column(db.Integer, db.ForeignKey('users.id'))
    place9 = db.Column(db.Integer, db.ForeignKey('users.id'))
    place10 = db.Column(db.Integer, db.ForeignKey('users.id'))
    
    def count_places(self):
        n = 0
        if self.place1  is None:
            n += 1
        if self.place2  is None:
            n += 1
        if self.place3  is None:
            n += 1
        if self.place4  is None:
            n += 1
        if self.place5  is None:
            n += 1
        if self.place6  is None:
            n += 1
        if self.place7  is None:
            n += 1
        if self.place8  is None:
            n += 1
        if self.place9  is None:
            n += 1
        if self.place10 is None:
            n += 1
        return n

    def take_seats(self, userid, nbplaces_str):
        """
        La méthode change l'user qui a pris les places
        userid = Id user prenant la place
        nbplaces_str = nombre de places prises, variables prises direct depuis la db
        """
        nbplaces = int(nbplaces_str)
        if self.place1 is None and nbplaces > 0:
            self.place1 = userid
            nbplaces -= 1 
        if self.place2 is None and nbplaces > 0:
            self.place2 = userid
            nbplaces -= 1 
        if self.place3 is None and nbplaces > 0:
            self.place3 = userid
            nbplaces -= 1 
        if self.place4 is None and nbplaces > 0:
            self.place4 = userid
            nbplaces -= 1 
        if self.place5 is None and nbplaces > 0:
            self.place5 = userid
            nbplaces -= 1 
        if self.place6 is None and nbplaces > 0:
            self.place6 = userid
            nbplaces -= 1 
        if self.place7 is None and nbplaces > 0:
            self.place7 = userid
            nbplaces -= 1 
        if self.place8 is None and nbplaces > 0:
            self.place8 = userid
            nbplaces -= 1 
        if self.place9 is None and nbplaces > 0:
            self.place9 = userid
            nbplaces -= 1 
        if self.place10 is None and nbplaces > 0:
            self.place10= userid
            nbplaces -= 1

    def untake_seats(self, userid, places_user):
        """
        Remove  seats and return number of places untook
        """
        places_removed =0
        if str(self.place1) == str(userid):
            self.place1 = None
            places_removed +=1
        if str(self.place2)==str(userid):
            self.place2=None
            places_removed +=1
        if str(self.place3)==str(userid):
            self.place3=None
            places_removed +=1
        if str(self.place4)==str(userid):
            self.place4=None
            places_removed +=1
        if str(self.place5)==str(userid):
            self.place5=None
            places_removed +=1
        if str(self.place6)==str(userid):
            self.place6=None
            places_removed +=1
        if str(self.place6)==str(userid):
            self.place6=None
            places_removed +=1
        if str(self.place7)==str(userid):
            self.place7=None
            places_removed +=1
        if str(self.place8)==str(userid):
            self.place8=None
            places_removed +=1
        if str(self.place9)==str(userid):
            self.place9=None
            places_removed +=1
        if str(self.place10)==str(userid):
            self.place10=None
            places_removed +=1
        return places_removed
    
    def get_fullname(self, id_diplome):
        diplome = User.query.filter_by(id=id_diplome).first()
        if diplome is not None:
            nom_diplome = diplome.prenom +" "+ diplome.nom
            return nom_diplome
        else:
            return ''

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id'     : self.id,
            'place1' : self.get_fullname(self.place1),
            'place2' : self.get_fullname(self.place2),
            'place3' : self.get_fullname(self.place3),
            'place4' : self.get_fullname(self.place4),
            'place5' : self.get_fullname(self.place5),
            'place6' : self.get_fullname(self.place6),
            'place7' : self.get_fullname(self.place7),
            'place8' : self.get_fullname(self.place8),
            'place9' : self.get_fullname(self.place9),
            'place10' : self.get_fullname(self.place10)
        }
