from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, BooleanField, SubmitField, ValidationError
from wtforms.validators import DataRequired, Email, Length, EqualTo
from ..models import User



class EditProfile(FlaskForm):
    prenom = StringField('Prénom')
    nom = StringField('Nom')
    old_password = PasswordField('Type your old password')
    new_password = PasswordField('Type your new password', validators=[
         EqualTo('confirm')])
    confirm = PasswordField('Repeat Password', validators=[
                             EqualTo('new_password')])
    submit = SubmitField()