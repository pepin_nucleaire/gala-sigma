from flask import render_template, redirect, request, url_for, flash, Blueprint, current_app, Response, jsonify
from flask_login import login_user, logout_user, login_required, current_user
from .. import db
from ..models import User, PlanDeTable
import csv
import json
# Preinscription
from random import choice
import string
from datetime import datetime

bp = Blueprint('admin', __name__, template_folder='templates',
               url_prefix="/admin")


def check_admin():
    user = current_user
    if user.admin:
        pass
    else:
        flash("You're not an admin.")
        return redirect(url_for('main.index'))


@bp.route('/', methods=['GET'])
@login_required
def admin():
    check_admin()
    return render_template('admin.html', title='Administration')

def get_plan():
    plan_export = []
    plan = PlanDeTable.query.all()
    return plan


@bp.route('/api-plan')
def api_plan():
    plan = [ table.serialize for table in get_plan() ]
    return jsonify(plan)

@bp.route('/api-plan-uniq')
def api_plan_uniq():
    plan = get_plan()
    liste=[]
    for table in plan:
        for place in [table.place1,table.place2,table.place3,table.place4,table.place5,table.place6,table.place7,table.place8,table.place9,table.place10]:
            if place is not None:
                user = User.query.filter_by(id=place).first()
                if user.serialize not in liste:
                    liste.append(user.serialize)
    return jsonify(liste)
