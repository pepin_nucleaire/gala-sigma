from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField
from wtforms.validators import NumberRange, DataRequired


class PrisedePlaces(FlaskForm):
    nb_places = SelectField(
        'Nombre de places prises',
        validators=[DataRequired()],
        choices= [(1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10)])
    submit = SubmitField('Prendre une nouvelle table')
