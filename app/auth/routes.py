from flask import render_template, redirect, request, url_for, flash, Blueprint, current_app
from flask_login import login_user, logout_user, login_required, current_user
from .. import db
from ..models import User
from .forms import RegisterForm, LoginForm, ChangePassword, ForgotPassword
from itsdangerous import Serializer, URLSafeTimedSerializer
from ..email import send_email
from datetime import datetime

bp = Blueprint('auth', __name__, template_folder='templates')


@bp.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            new_user = User(form.email.data, form.password.data, form.prenom.data, form.nom.data)
            db.session.add(new_user)
            db.session.commit()
            send_confirmation_email(new_user.email)
            flash(
                'Thanks for registering!  Please check your email to confirm your email address.', 'success')
            return redirect(url_for('main.index'))
    return render_template('register.html', form=form, title='Register Page')


@bp.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.filter_by(email=form.email.data).first()
            if user is not None:
                if user.check_password(form.password.data) and user.email_confirmed:
                    user.authenticated = True
                    db.session.add(user)
                    db.session.commit()
                    login_user(user)
                    flash('Thanks for logging in, {}'.format(current_user.email))
                    return redirect(url_for('main.index'))
                elif not user.email_confirmed:
                    flash('Email not confirmed, Please verify your mailbox', 'error')
                elif not user.check_password(form.password.data):
                    flash('Incorrect password', 'error')
            else:
                flash('ERROR! Incorrect login credentials.', 'error')
    return render_template('login.html', form=form, title='Login Page')


@bp.route('/logout')
@login_required
def logout():
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    flash('Goodbye!', 'info')
    return redirect(url_for('main.index'))




@bp.route('/confirm/<token>')
def confirm_email(token):
    try:
        confirm_serializer = URLSafeTimedSerializer(
            current_app.config['SECRET_KEY'])
        email = confirm_serializer.loads(
            token, salt='email-confirmation-salt', max_age=3600)
    except:
        flash('The confirmation link is invalid or has expired.', 'error')
        return redirect(url_for('auth.login'))

    user = User.query.filter_by(email=email).first()

    if user.email_confirmed:
        flash('Account already confirmed. Please login.', 'info')
    else:
        user.email_confirmed = True
        user.email_confirmed_on = datetime.now()
        db.session.add(user)
        db.session.commit()
        flash('Thank you for confirming your email address!')

    return redirect(url_for('main.index'))


@bp.route('/update_pwd/<token>', methods=['GET', 'POST'])
def update_pwd(token):
    try:
        confirm_serializer = URLSafeTimedSerializer(
            current_app.config['SECRET_KEY'])
        email = confirm_serializer.loads(
            token, salt='email-confirmation-salt', max_age=3600)
    except:
        flash('The confirmation link is invalid or has expired.', 'error')
        return redirect(url_for('auth.login'))

    form = ChangePassword(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.filter_by(email=email).first()
            user.set_password(form.new_password.data)
            db.session.add(user)
            db.session.commit()
            flash('Password updated !')
    return render_template('update_password.html', form=form)


def send_confirmation_email(user_email):
    confirm_serializer = URLSafeTimedSerializer(
        current_app.config['SECRET_KEY'])

    confirm_url = url_for(
        'auth.confirm_email',
        token=confirm_serializer.dumps(
            user_email, salt='email-confirmation-salt'),
        _external=True)

    html = render_template(
        'email_confirmation.html',
        confirm_url=confirm_url)

    send_email('Confirm Your Email Address', [user_email], html)


@bp.route('/forgotpwd', methods=['GET', 'POST'])
def forgotpwd():
    form = ForgotPassword(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            email = form.email.data
            if User.query.filter_by(email=email).first():
                send_confirmation_password_email(email)
                flash(
                    'Email de confirmation de changement de mot de passe envoyé à ' + email, 'success')
                return redirect(url_for('main.index'))
            else:
                flash('Compte non existant')
    return render_template('forgot_password.html', form=form, title='Forgot Password')


def send_confirmation_password_email(user_email):
    confirm_serializer = URLSafeTimedSerializer(
        current_app.config['SECRET_KEY'])

    confirm_url = url_for(
        'auth.update_pwd',
        token=confirm_serializer.dumps(
            user_email, salt='email-confirmation-salt'),
        _external=True)

    html = render_template(
        'email_pwd_confirmation.html',
        confirm_url=confirm_url)

    send_email('Confirm Your Email Address', [user_email], html)
