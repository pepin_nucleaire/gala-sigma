from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, BooleanField, SubmitField, ValidationError
from wtforms.validators import DataRequired, Email, Length, EqualTo
from ..models import User


class RegisterForm(FlaskForm):
    prenom = StringField('Prénom', validators=[
                        DataRequired()])
    nom = StringField('Nom', validators=[
                        DataRequired()])
    email = StringField('Email', validators=[
                        DataRequired(), Email(), Length(min=6, max=200)])
    password = PasswordField('Password', validators=[
                             DataRequired(), Length(min=6, max=100)])
    confirm = PasswordField('Repeat Password', validators=[
                            DataRequired(), EqualTo('password')])
    submit = SubmitField()

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[
        DataRequired(), Email(), Length(min=6, max=200)])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField()


class ChangePassword(FlaskForm):
    new_password = PasswordField('Type your new password', validators=[
        DataRequired(), EqualTo('confirm')])
    confirm = PasswordField('Repeat Password', validators=[
                            DataRequired(), EqualTo('new_password')])
    submit = SubmitField()


class ForgotPassword(FlaskForm):

    email = StringField('Email', validators=[
                        DataRequired(), Email(), Length(min=6, max=200)])
    submit = SubmitField()
