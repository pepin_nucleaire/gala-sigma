import os
import sys

path = '/flask'
if path not in sys.path:
    sys.path.insert(0, path)

from app import create_app  # noqa

application = create_app('production')
