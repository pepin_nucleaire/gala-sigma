import time
import unittest

from app import create_app, db
from app.models import User


class UserModelTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.drop_all()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        # db.drop_all()
        self.app_context.pop()

    def test_set_password(self):
        u = User(email='cat@cat.fr', password='cat')
        self.assertTrue(u.password is not None)

    def test_pass_verification(self):
        u = User(email='cat@cat.fr', password='cat')
        self.assertTrue(u.check_password('cat'))
        self.assertFalse(u.check_password('dog'))

    def test_password_salt(self):
        u1 = User(email='cat1@cat.fr', password='cat')
        u2 = User(email='cat2@cat.fr', password='cat')
        self.assertTrue(u1.password != u2.password)

    def test_register(self):
        u1 = User(email='cat1@cat.fr', password='catcatcat')
        db.session.add(u1)
        db.session.commit()
        self.assertTrue(User.query.get(u1.email) is not None)
