import os

# import sys
# sys.path.insert(0, '/flask/run.py')

from flask_migrate import Migrate, upgrade  # noqa
from app import create_app, db  # noqa

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
migrate = Migrate(app, db)

